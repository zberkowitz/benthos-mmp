//! The MMP protocol is used to control a Teledyne Benthos acoustic modem product from another
//! computing device, be it a customer’s own computer platform such as a PC application or an
//! embedded AUV/ROV processor, or a Teledyne Benthos deck box display unit.
//!
//! Generally the protocol is designed to be agnostic with regard to the physical layer used to
//! carry it.  It can be implemented over a serial line as well as with acoustic packets.
//! Currently only the serial MMP interface is supported.
#![no_std]
pub mod command;
pub mod ffi;
pub mod parameter;

use core::convert::TryFrom;

/// Build an enum using constants defined in benthos-mmp-sys
///
/// Will generate:
///     - A public enum
///     - TryFrom<u8>, TryFrom<u16>, and TryFrom<u32> implementations
///
/// # Example
/// ```ignore
/// // `mmp_subsys_t` is the type mod exported by benthos-mmp-sys
/// //  Constants such as `MMP_SUBSYS_IFACE` and `MMP_SUBSYS_SREG` sill automatically
/// //  be expanded to `ffi::mmp_subsys_t::MMP_SUBSYS_IFACE` and
/// //  `ffi::mmp_subsys_t::MMP_SUBSYS_SREG`
///
/// mmp_enum! {
///     /// SubSystem identifier used in Get/Set/Notify messages
///     SubSystem : mmp_subsys_t {
///         /// MMP interface configuration and control data/notifications
///         IFace = MMP_SUBSYS_IFACE,
///         /// S-Registers
///         SReg = MMP_SUBSYS_SREG,
///         ...
///     }
/// }
/// ```
#[doc(hidden)]
#[macro_export]
macro_rules! mmp_enum {
    (
        $( #[$outer_meta: meta] )*
        $Name: ident : $Base:ident {
            $(
                $( #[$inner_meta: ident $($args:tt)*] )*
                $Variant: ident = $value: ident
            ),*
            $( , )?
        }
    ) => {
        $( #[$outer_meta] )*
        #[derive(Copy, Clone, PartialEq, Eq, Debug)]
        pub enum $Name {
            $(
                $( #[$inner_meta $($args)*] )*
                $Variant = ffi::$Base::$value as isize
            ),*
        }

        impl core::convert::TryFrom<u8> for $Name {
            type Error = u8;

            /// Convert from a u8
            ///
            /// # Errors
            /// Returns the original `value` parameter as an error if conversion fails.
            fn try_from(value: u8) -> Result<Self, Self::Error> {
                match value as ffi::$Base::Type {
                    $(
                        ffi::$Base::$value => Ok($Name::$Variant),
                    )*
                    _ => Err(value)
                }
            }
        }
        impl core::convert::TryFrom<u16> for $Name {
            type Error = u16;

            /// Convert from a u16
            ///
            /// # Errors
            /// Returns the original `value` parameter as an error if conversion fails.
            fn try_from(value: u16) -> Result<Self, Self::Error> {
                match value as ffi::$Base::Type {
                    $(
                        ffi::$Base::$value => Ok($Name::$Variant),
                    )*
                    _ => Err(value)
                }
            }
        }
        impl core::convert::TryFrom<u32> for $Name {
            type Error = u32;

            /// Convert from a u32
            ///
            /// # Errors
            /// Returns the original `value` parameter as an error if conversion fails.
            fn try_from(value: u32) -> Result<Self, Self::Error> {
                match value as ffi::$Base::Type {
                    $(
                        ffi::$Base::$value => Ok($Name::$Variant),
                    )*
                    _ => Err(value)
                }
            }
        }
    }
}

mmp_enum! {
    Error : mmp_iface_err_t {
        ///Null error code
        Null = MMP_IFACE_ERR_NULL,
        ///Operation has timed out
        Timeout = MMP_IFACE_ERR_TIMEOUT,
        ///Bad subsystem in GET/SET request
        BadSubSystem = MMP_IFACE_ERR_BAD_SUBSYS,
        ///Bad field in GET/SET request
        BadField = MMP_IFACE_ERR_BAD_FIELD,
        ///Bad value in GET/SET request
        BadValue = MMP_IFACE_ERR_BAD_VALUE,
        ///Error parsing serial stream
        Parse = MMP_IFACE_ERR_PARSE,
        ///Bad command section in EXEC request
        BadCommandSect = MMP_IFACE_ERR_BAD_CMDSECT,
        ///Bad command in EXEC request
        BadCommand = MMP_IFACE_ERR_BAD_CMD,
        ///Bad arguments to command in EXEC request
        BadCommandArgs = MMP_IFACE_ERR_BAD_CMDARGS,
        ///More than one EXEC operation in single transaction
        MultiCommand = MMP_IFACE_ERR_MULTICMD,
        ///Modem too busy to process request
        DeviceBusy = MMP_IFACE_ERR_DEVICE_BUSY,
        ///Field not accessible with GET/SET, spontaneous NOTIFY only
        NotifyOnly = MMP_IFACE_ERR_NOTIFY_ONLY,
        ///Resources not available to process request
        NoResource = MMP_IFACE_ERR_NO_RESOURCE,
        ///Value may not be modified with a SET
        Unmodifiable = MMP_IFACE_ERR_UNMODIFIABLE,
        ///Insufficient permission level to carry out operation
        Permission = MMP_IFACE_ERR_PERMISSION,
        ///The modem is not in a state that can validly process the request
        InvalidState = MMP_IFACE_ERR_INVALID_STATE,
        ///The requested operation requires feature key activation, but the key is not installed
        FeatureNotEnabled = MMP_IFACE_ERR_FEAT_NOT_ENABLED,
        ///The requested operation is recognized but is not yet implemented in the MMP engine
        NotImplemented = MMP_IFACE_ERR_NOT_IMPLEMENTED,
        ///The requested operation relies on hardware that is either not present on the board or did not initialize properly
        HwNotPresent = MMP_IFACE_ERR_HW_NOT_PRESENT,
        ///An attempt to modify a database (like the configuration) was disallowed because it is in a locked state
        DbLocked = MMP_IFACE_ERR_DB_LOCKED,
        ///The modification attempt cannot be performed acoustically 0x15 IFACE_ERR_SET_ONLY The field may only be SET, and not queried with a GET operation
        AcousticDisallowed = MMP_IFACE_ERR_ACOU_DISALLOWED,
    }
}
/// Start-of-message sentinel is the ‘@’ character, or ASCII 64 (0x40).
/// If the start-of-message sentinel is received, the modem’s MMP master agent
/// will begin looking for an MMP header followed by one or more parameter
/// groups.
pub const START_OF_MESSAGE_SENTINEL: u8 = ffi::mmp_sentinel_t::MMP_SENTINEL as u8;

/// Quit sentinel is control-d, or ASCII 04.
///
/// If the quit sentinel is received, the MMP daemon is exited and no further
/// preamble or parameters need be sent in.
pub const QUIT_SENTINEL: u8 = 0x04; // mentioned in docs

/// The four basic message types in the MMP serial protocol
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum MessageType {
    /// Used to obtain a piece of modem data
    Get,
    /// Used to modify a piece of modem data
    Set,
    /// Used to instruct the modem ot perform some non-trivial command
    Exec,
    /// Used to return data or command results from the modem
    Notify,
}

/// The normal XID range is from 1 through 255 (0xFF).
///
/// The controlling CPU sets this value in the requests it generates.
/// Whatever XID a GET/SET/EXEC request is sent in with, the corresponding
/// NOTIFY will return with.  A typical implementation would start the XID
/// at 1 and increment it for each new outbound request until it reached
/// 255, then wrap it around back to 1 again.
///
/// There is no requirement that the XIDs increment in any particular order
/// (or that they change at all).  They are only there as a means of tracking
/// NOTIFY responses against requests. o An XID value of 0 is known as the
/// Null Transaction ID.  It is used whenever the modem generates a spontaneous
/// notification that is not in response to a particular GET/SET/EXEC request.
///
/// It is not recommended that the controller-generated requests use the null transaction ID.
pub type TransactionId = u8;

/// Null Transaction ID.
/// It is used whenever the modem generates a spontaneous notification that is not in response
/// to a particular GET/SET/EXEC request.
///
/// It is not recommended that the controller-generated requests use the null transaction ID.
pub const NULL_TRANSACTION_ID: TransactionId = ffi::mmp_xid_sentinel_t::MMP_XID_NULL as u8;

/// Indicates if the message is a GET, SET, EXEC or NOTIFY.
pub type OperationId = u8;

impl core::convert::TryFrom<OperationId> for MessageType {
    type Error = u8;

    /// Try to convert an OperationId to a MessageKind
    /// # Examples
    /// ```
    /// use benthos_mmp::MessageType;
    /// use core::convert::TryFrom;
    ///
    /// assert_eq!(MessageType::try_from(b'g'), Ok(MessageType::Get));
    /// assert_eq!(MessageType::try_from(b's'), Ok(MessageType::Set));
    /// assert_eq!(MessageType::try_from(b'x'), Ok(MessageType::Exec));
    /// assert_eq!(MessageType::try_from(b'n'), Ok(MessageType::Notify));
    ///
    /// // An invalid OperationId returns an error
    /// assert_eq!(MessageType::try_from(b'z'), Err(b'z'))
    /// ```
    fn try_from(id: OperationId) -> Result<Self, Self::Error> {
        match id as ffi::mmp_type_t::Type {
            ffi::mmp_type_t::MMP_GET => Ok(MessageType::Get),
            ffi::mmp_type_t::MMP_SET => Ok(MessageType::Set),
            ffi::mmp_type_t::MMP_EXECUTE => Ok(MessageType::Exec),
            ffi::mmp_type_t::MMP_NOTIFY => Ok(MessageType::Notify),
            _ => Err(id),
        }
    }
}
/// Number of parameter groups (subsystem/field pairs & payloads) the operation contains.
///
/// EXEC operations cannot be ganged together with other GETs/SETs and there may only
/// be one EXEC parameter group per request (i.e. the parameter count MUST be 1 for
/// EXECs).
pub type ParameterGroupCount = u8;

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Preamble {
    /// Transaction ID
    pub transaction_id: TransactionId,
    /// Operation Type
    pub operation_type: MessageType,
    /// Number of parameter groups
    pub parameter_group_count: ParameterGroupCount,
}

/// Parse a preamble from from the input stream of bytes
///
/// The preamble in this context does NOT include the starting '@' sentinel
/// # Examples
/// ```
/// use benthos_mmp::*;
///
/// assert_eq!(parse_preamble(b"\x01g\x04"), Ok((&b""[..], Preamble{transaction_id: 1, operation_type: MessageType::Get, parameter_group_count: 4})));
/// assert_eq!(parse_preamble(b"\xF1x\x01"), Ok((&b""[..], Preamble{transaction_id: 0xF1, operation_type: MessageType::Exec, parameter_group_count: 1})));
/// ```
pub fn parse_preamble(i: &[u8]) -> nom::IResult<&[u8], Preamble> {
    nom::combinator::map(
        nom::sequence::tuple((
            nom::number::complete::le_u8,
            nom::combinator::map_res(nom::number::complete::le_u8, MessageType::try_from),
            nom::number::complete::le_u8,
        )),
        |(transaction_id, operation_type, parameter_group_count)| Preamble {
            transaction_id,
            operation_type,
            parameter_group_count,
        },
    )(i)
}
