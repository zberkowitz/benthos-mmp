use crate::ffi;
use crate::mmp_enum;

use core::convert::TryFrom;

mmp_enum! {
    /// SubSystem identifier used in Get/Set/Notify messages
    SubSystem : mmp_subsys_t {
        /// MMP interface configuration and control data/notifications
        IFace = MMP_SUBSYS_IFACE,
        /// S-Registers
        SReg = MMP_SUBSYS_SREG,
         /// Deck box-specific data/notifications
        Deckbox = MMP_SUBSYS_DECKBOX,
         /// Data packets and other general notifications
        Data = MMP_SUBSYS_DATA,
         /// Transponding and ranging data/notifications
        Transpond = MMP_SUBSYS_TRANSPOND,
         /// Time- and date-related data/notifications
        Time = MMP_SUBSYS_TIME,
        /// Special subsystem for retrieving all values in a config section: set the field with the CFG section number to get or MMP_SUBSYS_CFG_ALL to get all sections.  If used with a SET operation and MMP_SUBSYS_CFG_ALL is used for both the the subsys and field, the configuration database will be written to flash.
        CfgAll = MMP_SUBSYS_CFG_ALL,
         /// Co-processor board
        CfgCoproc = CFG_SECT_COPROC,
         /// On-board datalogger
        CfgDatalog = CFG_SECT_DATALOG,
         /// Modem functionality
        CfgModem = CFG_SECT_MODEM,
         /// Release functionality
        CfgRelease = CFG_SECT_RELEASE,
         /// UART paramters
        CfgSerial = CFG_SECT_SERIAL,
         /// System parameters
        CfgSystem = CFG_SECT_SYSTEM,
         /// Test functionality
        CfgTest = CFG_SECT_TEST,
         /// Version information
        CfgVersion = CFG_SECT_VERSION,
         /// Transpond control
        CfgXpnd = CFG_SECT_XPND,
         /// Global Pose Sensors
        CfgNav = CFG_SECT_NAV,
         /// Data recorder configuration
        CfgRecorder = CFG_SECT_RECORDER,
         /// Sonar Modem configuration
        CfgSonar = CFG_SECT_SONAR,
         /// Sonar Modem User configuration
        CfgSdk = CFG_SECT_USER_SDK,
         /// Transport layer routing/tagging information
        CfgTransport = CFG_SECT_TRANSPORT,
         /// WHOI configuration
        CfgWhoi = CFG_SECT_WHOI,
         /// Analog Input Option
        CfgAin = CFG_SECT_AIN,
         /// Pressure gauge options (requires analog input)
        CfgPressure = CFG_SECT_PRESSURE,
         /// USBL control section
        CfgUsbl = CFG_SECT_USBL,
         /// NUWC Ranging DPSK Integration section
        CfgDpsk = CFG_SECT_DPSK,
    }
}

mmp_enum! {
    /// IFACE SubSystem field flags
    IFaceField : mmp_iface_fld_t {
        /// Status of MMP subsystem
        MmpStat = MMP_IFACE_FLD_MMPSTAT,
         /// MMP error notification
        Err = MMP_IFACE_FLD_ERR,
         /// Version of MMP protocol running
        Version = MMP_IFACE_FLD_VERSION,
         /// MMP command (EXECUTE) result notification
        CmdResult = MMP_IFACE_FLD_CMDRESULT,
         /// Block spontaneous notifications from one or more subsystems.  Used with SET, provide a variable length list of subsystems to block.  Returns a variable-length list of blocked subsystems.  Note that responses will always be received when induced by a direct SET/CMD on an interface even if the subsystem is blocked; this only filters spontaneous notifies.
        BlockNotifiy = MMP_IFACE_FLD_BLOCKNOTIFY,
         /// Allow spontaneous notifications from one or more subsystems.  Used with SET, provide a variable-length list of subsystems to unblock.  Returns a variable-length list of unblocked subsystems.
        UnblockNotify = MMP_IFACE_FLD_UNBLOCKNOTIFY,
         /// DSP firmware version currently running
        SwVersion = MMP_IFACE_FLD_DSP_SW_VERSION,
         /// Privilege level on this MMP interface (requires password only if elevating from current privlev)
        PrivLev = MMP_IFACE_FLD_PRIVLEV,
         /// temporarily enable a feature key
        FeatureKey = MMP_IFACE_FLD_FEATURE_KEY,
         /// Obtain the assembly serial number
        SerialNo = MMP_IFACE_FLD_UNIT_SERNO,
         /// Enable, disable, or query the status of appended notification counter (unique on a perinterface basis)
        NotifyCount = MMP_IFACE_FLD_NTFY_COUNT,
         /// Enable, disable, or query the status of appended notification XOR checksum
        NotifyChecksum = MMP_IFACE_FLD_NTFY_CKSUM,
         /// Set an AES user key for use with encrypted code images, along with an optional timeout.  If 0xFFFF is provided for the timeout value, the AES key and auto-clear timeouts are cleared.  An INVALID_STATE error will result if an attempt is made to install a new key over an existing one without first clearing it.
        AESUserKey = MMP_IFACE_FLD_AES_USER_KEY,
          /// A compatibility number used for syncing external controllers (such as ReleaseIT display) with a given range of software version.  This number may vary by platform and is intended primarily for Teledyne use.
        Compatibility = MMP_IFACE_FLD_COMPATIBILITY,
    }
}

mmp_enum! {
    SRegField : mmp_sreg_fld_t {
     /// DSP SW version
        Version = MMP_SREG_FLD_VERSION,
         /// Positive acknowledgements
        PositiveAck = MMP_SREG_FLD_POSACK,
         /// Serial port baud & configuration
        SerialBaud = MMP_SREG_FLD_SERBAUD,
         /// Acoustic baud rate
        AcousticBaud = MMP_SREG_FLD_ACOUBAUD,
         /// Transmit power
        TxPower = MMP_SREG_FLD_TXPOWER,
         /// Acoustic response timeout
        ResponseTimeout = MMP_SREG_FLD_RESPTIMEOUT,
         /// Packet forwarding delay
        FwdDelay = MMP_SREG_FLD_FWDDELAY,
         /// PSK coprocessor enable/status
        CoProc = MMP_SREG_FLD_COPROC,
         /// Low-power idle timeout
        LowPowerTimeout = MMP_SREG_FLD_LPTIMEOUT,
         /// Serial port flow control
        SerialFlowCtrl = MMP_SREG_FLD_FLOWCTRL,
         /// Acoustic test message length
        TestMsgLen = MMP_SREG_FLD_TESTMSGLEN,
         /// Console message verbosity
        Verbosity = MMP_SREG_FLD_VERBOSITY,
         /// Default remote modem address
        ModemRemoteAddress = MMP_SREG_FLD_REMADDR,
         /// Local modem address
        ModemLocalAddress = MMP_SREG_FLD_LOCADDR,
         /// Receive sensitivity threshold for transpond pings
        RxThreshold = MMP_SREG_FLD_RXTHRESHOLD,
         /// Acoustic band
        Band = MMP_SREG_FLD_BAND,
         /// Transpond interrogation ping pulse width
        TxPulseWidth = MMP_SREG_FLD_TXPULSEWIDTH,
         /// Transpond received ping pulse width
        RxPulseWidth = MMP_SREG_FLD_RXPULSEWIDTH,
         /// Transponder/ranging turn-around time
        TurnAroundTime = MMP_SREG_FLD_TAT,
         /// Internal/external 1 PPS time sync mode
        PpsSync = MMP_SREG_FLD_PPSSYNC,
         /// Transpond ping receive frequency (only for units that can't receive multiple frequencies)
        RxFreq = MMP_SREG_FLD_RXFREQ,
         /// Transpond ping lockout time
        XpondLockoutTime = MMP_SREG_FLD_XPNDLOCKOUT,
         /// Use to SET/GET all S-registers at once
        All = MMP_SREG_FLD_ALL,
    }
}

mmp_enum! {
    /// Deckbox subsystem fields
    DeckboxField : mmp_deckbox_fld_t {
     /// Speaker volume
        SpeakerVol = MMP_DECKBOX_FLD_SPKRVOL,
         /// Headphones volume
        HeadphoneVol = MMP_DECKBOX_FLD_PHONESVOL,
         /// Power source information
        PowerSense = MMP_DECKBOX_FLD_PWRSENSE,
         /// Internal battery level (rough percentage); a 'critical' battery level will trigger a spontaneous MMP notification
        BatteryLevel = MMP_DECKBOX_FLD_BATTLEV,
         /// Internal battery voltage
        BatteryVoltage = MMP_DECKBOX_FLD_BATTVOLT,
         /// Deck box model
        Model = MMP_DECKBOX_FLD_MODEL,
    }
}

mmp_enum! {
    /// Data subsystem fields
    DataField : mmp_data_fld_t {
     /// Data ACK from remote modem received
        Ack = MMP_DATA_FLD_ACK,
         /// Data packet from remote modem received
        RemoteData = MMP_DATA_FLD_REMOTE_DATA,
         /// Overtemp condition on transmit
        OverTemp = MMP_DATA_FLD_XMIT_OVERTEMP,
         /// Release burn-wire burn cycle active, tilt detected (unit release)
        BurnComplete = MMP_DATA_FLD_BURN_COMPLETE,
         /// Information on T/R board and RCV module
        TrRcvStatus = MMP_DATA_FLD_TRRC_STATUS,
         /// Information on feature authorization keys
        KeyStatus = MMP_DATA_FLD_FEAT_KEY_STATUS,
         /// Header information on packets received
        RemoteHeader = MMP_DATA_FLD_REMOTE_HEADER,
         /// Timestamp notification for a rx or tx event
        Timestamp = MMP_DATA_FLD_TIMESTAMP,
         /// Doppler speed information from tones
        Doppler = MMP_DATA_FLD_DOPPLER,
         /// Release burn-wire cycle timed out with no tilt
        BurnTimeout = MMP_DATA_FLD_BURN_TIMEOUT,
         /// Floating point co-processor module version
        FPModuleVersion = MMP_DATA_FLD_FPM_VERSION,
         /// Notification that a PSK packet was received but FPM coprocessor is not present/enabled.
        PskPktNoCoProc = MMP_DATA_FLD_PSK_PKT_NO_COPROC,
         /// Notification of the modem entering or exiting low power mode
        LowPower = MMP_DATA_FLD_LOW_POWER,
         /// Acoustic statistics for the packet being received
        AcousticStats = MMP_DATA_FLD_ACSTATS,
         /// Data packet from remote modem containing data logger data, with CRC flags
        DataLogBlock = MMP_DATA_FLD_REMOTE_DLOGBLK,
         /// Header received from remote modem with errors; contains acoustic statistics if header decoded, or sentinel value to indicate low SNR acquisition detected
        HeaderError = MMP_DATA_FLD_HEADER_ERROR,
         /// Status of navigation sources (location, heading, attitude)
        NavStatus = MMP_DATA_FLD_NAV_STATUS,
         /// Updated range to a remote node determined via acoustic communication
        RangeUpdate = MMP_DATA_FLD_RANGE_UPDATE,
          /// Indication of whether a floating point coprocessor that was configured to be present at boot time failed to initialize.  Will always return 0 on platforms that don't support FPM coprocessors.
        FPModuleBootFailure = MMP_DATA_FLD_FPM_BOOT_FAIL,
         /// Gives the RCV module attenuator steps and total gain
        RcvGain = MMP_DATA_FLD_RCV_GAIN,
         /// Indicates activation or deactivation of the spectrum mode, along with some parameters
        SpectrumStatus= MMP_DATA_FLD_SPECTRUM_STATUS,
         /// Energy levels for frequency bins calculated during spectrum mode
        SpectrumData = MMP_DATA_FLD_SPECTRUM_DATA,
    }
}

mmp_enum! {
    /// Transpond subsystem flags
    TranspondField : mmp_transpond_fld_t {
     /// Transpond mode status
        Status = MMP_TRANSPOND_FLD_STAT,
         /// Transpond ping received
        PingRecvd = MMP_TRANSPOND_FLD_PINGRCVD,
         /// Receive sensitivity adjustment for channel 0
        ChannelRxAdj0 = MMP_TRANSPOND_FLD_CHNLRXADJ_0,
         /// Receive sensitivity adjustment for channel 1
        ChannelRxAdj1 = MMP_TRANSPOND_FLD_CHNLRXADJ_1,
         /// Receive sensitivity adjustment for channel 2
        ChannelRxAdj2 = MMP_TRANSPOND_FLD_CHNLRXADJ_2,
         /// Receive sensitivity adjustment for channel 3
        ChannelRxAdj3 = MMP_TRANSPOND_FLD_CHNLRXADJ_3,
         /// Receive sensitivity adjustment for channel 4
        ChannelRxAdj4 = MMP_TRANSPOND_FLD_CHNLRXADJ_4,
         /// Receive sensitivity adjustment for channel 5
        ChannelRxAdj5 = MMP_TRANSPOND_FLD_CHNLRXADJ_5,
         /// Receive sensitivity adjustment for channel 6
        ChannelRxAdj6 = MMP_TRANSPOND_FLD_CHNLRXADJ_6,
         /// Receive sensitivity adjustment for channel 7
        ChannelRxAdj7 = MMP_TRANSPOND_FLD_CHNLRXADJ_7,
         /// Receive sensitivity adjustment for channel 8
        ChannelRxAdj8 = MMP_TRANSPOND_FLD_CHNLRXADJ_8,
         /// Receive sensitivity adjustment for channel 9
        ChannelRxAdj9 = MMP_TRANSPOND_FLD_CHNLRXADJ_9,
         /// Receive sensitivity adjustment for channel 10
        ChannelRxAdj10 = MMP_TRANSPOND_FLD_CHNLRXADJ_10,
         /// Receive sensitivity adjustment for channel 11
        ChannelRxAdj11 = MMP_TRANSPOND_FLD_CHNLRXADJ_11,
         /// Receive sensitivity adjustment for channel 12
        ChannelRxAdj12 = MMP_TRANSPOND_FLD_CHNLRXADJ_12,
         /// Receive sensitivity adjustment for channel 13
        ChannelRxAdj13 = MMP_TRANSPOND_FLD_CHNLRXADJ_13,
         /// Receive sensitivity adjustment for channel 14
        ChannelRxAdj14 = MMP_TRANSPOND_FLD_CHNLRXADJ_14,
         /// Receive sensitivity adjustment for channel 15
        ChannelRxAdj15 = MMP_TRANSPOND_FLD_CHNLRXADJ_15,
         /// Receive sensitivity adjustment for channel 16
        ChannelRxAdj16 = MMP_TRANSPOND_FLD_CHNLRXADJ_16,
         /// Receive sensitivity adjustment for channel 17
        ChannelRxAdj17 = MMP_TRANSPOND_FLD_CHNLRXADJ_17,
         /// Receive sensitivity adjustment for channel 18
        ChannelRxAdj18 = MMP_TRANSPOND_FLD_CHNLRXADJ_18,
         /// Receive sensitivity adjustment for channel 19
        ChannelRxAdj19 = MMP_TRANSPOND_FLD_CHNLRXADJ_19,
         /// Receive sensitivity adjustment for channel 20
        ChannelRxAdj20 = MMP_TRANSPOND_FLD_CHNLRXADJ_20,
         /// Receive sensitivity adjustment for channel 21
        ChannelRxAdj21 = MMP_TRANSPOND_FLD_CHNLRXADJ_21,
         /// Receive sensitivity adjustment for channel 22
        ChannelRxAdj22 = MMP_TRANSPOND_FLD_CHNLRXADJ_22,
         /// Receive sensitivity adjustment for channel 23
        ChannelRxAdj23 = MMP_TRANSPOND_FLD_CHNLRXADJ_23,
         /// Receive sensitivity adjustment for channel 24
        ChannelRxAdj24 = MMP_TRANSPOND_FLD_CHNLRXADJ_24,
         /// Receive sensitivity adjustment for channel 25
        ChannelRxAdj25 = MMP_TRANSPOND_FLD_CHNLRXADJ_25,
         /// Receive sensitivity adjustment for channel 26
        ChannelRxAdj26 = MMP_TRANSPOND_FLD_CHNLRXADJ_26,
         /// Receive sensitivity adjustment for channel 27
        ChannelRxAdj27 = MMP_TRANSPOND_FLD_CHNLRXADJ_27,
         /// Receive sensitivity adjustment for channel 28
        ChannelRxAdj28 = MMP_TRANSPOND_FLD_CHNLRXADJ_28,
         /// Receive sensitivity adjustment for channel 29
        ChannelRxAdj29 = MMP_TRANSPOND_FLD_CHNLRXADJ_29,
         /// Receive sensitivity adjustment for channel 30
        ChannelRxAdj30 = MMP_TRANSPOND_FLD_CHNLRXADJ_30,
         /// Receive sensitivity adjustment for channel 31
        ChannelRxAdj31 = MMP_TRANSPOND_FLD_CHNLRXADJ_31,
         /// Receive sensitivity adjustment for channel 32
        ChannelRxAdj32 = MMP_TRANSPOND_FLD_CHNLRXADJ_32,
         /// Receive sensitivity adjustment for channel 33
        ChannelRxAdj33 = MMP_TRANSPOND_FLD_CHNLRXADJ_33,
         /// Receive sensitivity adjustment for channel 34
        ChannelRxAdj34 = MMP_TRANSPOND_FLD_CHNLRXADJ_34,
         /// Receive sensitivity adjustment for channel 35
        ChannelRxAdj35 = MMP_TRANSPOND_FLD_CHNLRXADJ_35,
         /// Receive sensitivity adjustment for channel 36
        ChannelRxAdj36 = MMP_TRANSPOND_FLD_CHNLRXADJ_36,
         /// Receive sensitivity adjustment for channel 37
        ChannelRxAdj37 = MMP_TRANSPOND_FLD_CHNLRXADJ_37,
         /// Receive sensitivity adjustment for channel 38
        ChannelRxAdj38 = MMP_TRANSPOND_FLD_CHNLRXADJ_38,
         /// Receive sensitivity adjustment for channel 39
        ChannelRxAdj39 = MMP_TRANSPOND_FLD_CHNLRXADJ_39,
         /// Receive sensitivity adjustment for channel 40
        ChannelRxAdj40 = MMP_TRANSPOND_FLD_CHNLRXADJ_40,
        /// Set or get all receive channel sensitivity adjustments at once
        ChannelRxAdjAll = MMP_TRANSPOND_FLD_CHNLRXADJ_ALL,
    }
}

mmp_enum! {
    /// Time subsystem flags
    TimeField : mmp_time_fld_t {
     /// (currently unsupported) Arrival of 1 PPS synchronization pulse
        PpsPulse = MMP_TIME_FLD_1PPS_PULSE,
         /// The time and date on the modem's clock
        TimeDate = MMP_TIME_FLD_TIMEDATE,
         /// (currently unsupported) Parameters governing Daylight Savings Time adjustments of local time
        DstParms = MMP_TIME_FLD_DSTPARMS,
         /// Battery level on modem's clock
        RtcBattery = MMP_TIME_FLD_RTCBATT,
    }
}

mmp_enum! {
    /// Coprocessor config subsystem flags
    ConfigCoprocField: cfg_coproc_param_enum_t {
       /// Indicate power mode of coprocessor board
        CoProcStatus = CFG_COPROC_CPBOARD,
         /// PSK number of forward taps
        NumFwdTaps = CFG_COPROC_FDFWDTAPS,
         /// PSK number of backward taps
        NumBwdTaps = CFG_COPROC_FDBCKTAPS,
    }
}

mmp_enum! {
    /// Datalogger config subsystem flags
    ConfigDataloggerField: cfg_datalog_param_enum_t {
        /// Log acoustically received data to datalogger
        AcousticData = CFG_DATALOG_ACDATA,
           /// Log acoustic statistics to datalogger
        AcousticStats = CFG_DATALOG_ACSTATS,
           /// Configure datalogger as flat (stop when full) or circular (oldest records discarded for newer)
        RingBuffer = CFG_DATALOG_RINGBUF,
           /// The record-partitioning mode for the data logger: time, character, or size based.
        LogMode = CFG_DATALOG_LOGMODE,
          /// The ASCII value of a sentinel character that will trigger closure of a discrete record in the data logger
        Sentinel = CFG_DATALOG_SENTINEL,
          /// The number of characters that must be reached in order to trigger closure of a discrete record in the data logger
        CharCount = CFG_DATALOG_CHRCOUNT,
          /// Indicates which storage medium the data logger points to (internal or external SDHC)
        LogStore = CFG_DATALOG_LOGSTORE,
    }
}

mmp_enum! {
    /// Modem configuration subsystem flags
    ConfigModemField : cfg_modem_param_enum_t {
     /// Acoustic data retry mode
        DataRetry = CFG_MODEM_DATARETRY,
         /// Acoustic response timeout
        ResponseTimeout = CFG_MODEM_ACRSPTMOUT,
         /// Operation mode
        OpMode = CFG_MODEM_OPMODE,
         /// Device enable line behavior
        DeviceEnable = CFG_MODEM_DEVENABLE,
         /// Data packet forwarding delay
        FwdDelay = CFG_MODEM_FWDDELAY,
         /// Modem local address
        ModemLocalAddress = CFG_MODEM_LOCALADDR,
         /// Print modem data hexadecimal
        ModemDataHex = CFG_MODEM_PRNTHEX,
         /// Modem default remote address
        ModemRemoteAddress = CFG_MODEM_REMOTEADDR,
         /// Receive packet type
        RcvPacketType = CFG_MODEM_RXPKTTYPE,
         /// Determine whether to process or discard data with bit errors
        ShowBadData = CFG_MODEM_SHOWBADDATA,
         /// Control playing of startup tones upon boot
        StartTones = CFG_MODEM_STARTTONES,
         /// Acoustic transmit bitrate of data
        TxRate = CFG_MODEM_TXRATE,
         /// Acoustic transmit power level
        TxPower = CFG_MODEM_TXPOWER,
         /// Control sending of wakeup preamble tones.
        WakeTones = CFG_MODEM_WAKETONES,
          /// Direct CLAM shell to behave in 'strict AT' (3rd gen style AT commands only) mode or not
        StrictATMode = CFG_MODEM_STRICTAT,
         /// Select the UART input mode for data transfer and data logging (single, dual)
        InputMode = CFG_MODEM_INPUTMODE,
         /// Control use of SmartRetry functionality for corrupted packet transmissions when data retries are enabled
        SmartRetry = CFG_MODEM_SMARTRETRY,
         /// Layer 2 Protocol selector
        Layer2Protocol = CFG_MODEM_L2PROTOCOL,
         /// Acoustic transmit bitrate rate of header
        HeaderRate = CFG_MODEM_HEADERRATE,
         /// Domain key that is used to create comms exclusivity groups
        DomainKey = CFG_MODEM_DOMAINKEY,
         /// Determine whether to enable auto detect of header or use setting in HeaderRate parm
        AutoDetectHdr = CFG_MODEM_AUTODETECTHDR,
         /// threshold adjustment for chirp detection
        ChirpThreshold = CFG_MODEM_CHIRP_THRESHOLD,
         /// Sets the maximum acoustic SPL output; the @TxPower parameter range is scaled according to this parameter.
        TxAtten = CFG_MODEM_TXATTEN,
         /// Moves LocalAddr to another group
        AddrGroup = CFG_MODEM_ADDRGROUP,
    }
}

mmp_enum! {
    ConfigReleaseField : cfg_release_param_enum_t {
     /// Duration (seconds) to play Benthos FSK release tones
        FSKReleaseToneDuration = CFG_RELEASE_FSKRLSDUR,
         /// Lost communication count (15 second increments)
        LostCommsCount = CFG_RELEASE_LSTCOMMSCNT,
         /// Release code to trigger this release (valid on release models only)
        ReleaseCode = CFG_RELEASE_RLSCODE,
         /// Timeout count for timed releases
        TimedReleaseTimeoutCount = CFG_RELEASE_TIMEDRELEASE,
         /// Type of motor used in this release; only valid for releases which can support differing motor type
        MotorType = CFG_RELEASE_MOTORTYPE,
         /// Minimun on time for Release Enable
        ReleaseEnableTimeMin = CFG_RELEASE_RLSMINENATIME,
         /// Maximin on time for Release Enable
        ReleaseEnableTimeMax = CFG_RELEASE_RLSMAXENATIME,
    }
}

mmp_enum! {
    /// Serial config subsystem flags
    ConfigSerialField : cfg_serial_param_enum_t {
         /// Port 1 baud rate
        Port1Baud = CFG_SERIAL_P1BAUD,
         /// Port 1 echo (full/half duplex)
        Port1Echo = CFG_SERIAL_P1ECHOCHAR,
          /// Port 1 flow control
        Port1FlowCtrl = CFG_SERIAL_P1FLOWCTL,
          /// Port 1 idle polarity
        Port1IdlePolarity = CFG_SERIAL_P1IDLEPOL,
         /// Port 1 comms protocol
        Port1LogicLevel = CFG_SERIAL_P1PROTOCOL,
          /// Port 1 strip bit 7
        Port1StripBit7 = CFG_SERIAL_P1STRIPB7,
          /// Port 1 not counted towards activity for sleep
        Port1NoSleep = CFG_SERIAL_P1NOSLEEP,
             /// Port 2 baud rate"
        Port2Baud = CFG_SERIAL_P2BAUD,
         /// Port 2 echo (full/half duplex)
        Port2Echo = CFG_SERIAL_P2ECHOCHAR,
          /// Port 2 flow control
        Port2FlowCtrl = CFG_SERIAL_P2FLOWCTL,
          /// Port 2 idle polarity
        Port2IdlePolarity = CFG_SERIAL_P2IDLEPOL,
          /// Port 2 strip bit 7
        Port2StripBit7 = CFG_SERIAL_P2STRIPB7,
          /// Port 2 not counted towards activity for sleep
        Port2NoSleep = CFG_SERIAL_P2NOSLEEP,
             /// Port 1 cooking mode
        Port1CookingMode = CFG_SERIAL_P1MODE,
             /// Port 2 cooking mode
        Port2CookingMode = CFG_SERIAL_P2MODE,
         /// Port 2 on RS232 or CMOS
        Port2LogicLevel = CFG_SERIAL_P2PROTOCOL,
          /// allow RTS lines to be off on lowpower, follow HW flow control or remain on
        LowPowerFlowControl = CFG_SERIAL_LPFLOWCTL,
    }
}

mmp_enum! {
    /// System config subsystem flags
    ConfigSystemField : cfg_system_param_enum_t {
     /// Enable auxiliary acoustic input
        AuxInputEnable = CFG_SYSTEM_AUXINP,
         /// Received data output in standard ASCII or as a hex byte dump
        RcvDataFormat = CFG_SYSTEM_ASCIIBIN,
          /// UNDOCUMENTED
        Bandwidth = CFG_SYSTEM_BANDWIDTH,
         /// Center carrier frequency (sets acoustic band)
        CarrierFreq = CFG_SYSTEM_CARRFREQ,
         /// Compact modem reset:
        ModemReset = CFG_SYSTEM_COMPMDMRST,
         /// Compact modem wakeup hibernate time (formerly last digit of S-10 when >300)
        WakeupHibernateTime = CFG_SYSTEM_CMWAKEHIB,
         /// Compact modem wakeup active listen time (formerly middle digit of S-10 when >400)
        ModemWakeListenTime = CFG_SYSTEM_CMWAKELISTEN,
         /// Half bandwidth modulation: 1 = Normal, 2 = half bandwidth
        HalfBandwidthModulation = CFG_SYSTEM_HALFBW,
         /// Frequency Hop receive threshold
        FreqHopThreshold = CFG_SYSTEM_FHTHRESH,
         /// Configuration of release type (factory-set)
        ReleaseType = CFG_SYSTEM_RLSTYPE,
         /// Mode for 1 PPS clock signal syncing
        SyncPps = CFG_SYSTEM_SYNCPPS,
         /// console status verbosity
        Verbosity = CFG_SYSTEM_VERBOSE,
          /// UNDOCUMENTED
        WakeThreshold = CFG_SYSTEM_WAKETHRESH,
         /// Control behavior of auxiliary acoustic output: either default behavior (speaker/phones for UDB, copies of samples for others), or force copies of outbound samples to aux port.
        AuxOut = CFG_SYSTEM_AUXOUT,
         /// Use an analog linear pot on EXT_SENSE1_ADC to control speaker and headphone volume
        VolCount = CFG_SYSTEM_VOLCONT,
         /// Enables fast wake from hibernate ability on compact modem (receiving side) and UDB (sending side).
        FastWake = CFG_SYSTEM_CMFASTWAKE,
         /// Low-power idle timer
        IdleTimer = CFG_SYSTEM_IDLETIMER,
         /// Prompt Setting
        Prompt = CFG_SYSTEM_PROMPT,
         /// copy RTC PPS out on BIN_OUT_0
        SyncCount = CFG_SYSTEM_SYNCOUT,
         /// BIN_IN_0 pullup
        PullUp0 = CFG_SYSTEM_PULLUP0,
         /// BIN_IN_1 pullup
        PullUp1 = CFG_SYSTEM_PULLUP1,
         /// The operating voltage threshold below which transmissions will be automatically reduced in power to prevent brown-out resets
        MinOpVoltage = CFG_SYSTEM_MINOPVOLT,
         /// The type of battery being used to power the modem (Standard alkaline, Smart Li+, Lithium primary, etc.)
        BatteryType = CFG_SYSTEM_BATTERYTYPE,
         /// The capacity of the installed battery pack in watt-hours.  Only for certain release products with battery monitoring capability.
        BatteryCapacity = CFG_SYSTEM_BATTERYCAPACITY,
         /// Manufacture date of the battery specifying month and year. Only for certain release products with battery monitoring capability.
        BatteryMfgDate = CFG_SYSTEM_BATTERYMFGDATE,
         /// The axis of the built-in accelerometer to be used as the tilt/pitch reference
        TiltAxis = CFG_SYSTEM_TILTAXIS,
         /// The Power On Timer used for Releases
        PwrOnTimer = CFG_SYSTEM_PWRONTIMER,
         /// The Awake Timer used for Releases
        AwakeTimer = CFG_SYSTEM_AWAKETIMER,
         /// The Acoustic Release Hibernate Sleep period
        AcousticReleaseHibernatePeriod = CFG_SYSTEM_ARWAKEHIB,
         /// hydrophone sensitivity in dB (uPa) for reference only
        ReleaseSensitivity = CFG_SYSTEM_RXSENS,
         /// keep TR board 12V and 3.3V on when in active receive
        AwakePower = CFG_SYSTEM_AWAKEPOWER,
    }
}

mmp_enum! {
    /// Test config subsystem flags
    ConfigTestField :cfg_test_param_enum_t {
     /// Test debug level
        DbgLevel = CFG_TEST_DBGLVL,
         /// Receive all packets (sniffer mode)
        RecvAll = CFG_TEST_RCVALL,
         /// Test response delay
        ResponseDelay = CFG_TEST_RSPDELAY,
         /// Test packet echo
        PacketEcho = CFG_TEST_PKTECHO,
         /// Test packet size
        PacketSize = CFG_TEST_PKTSIZE,
         /// Simulated variable acoustic delay, in milliseconds
        SimVariableDelay = CFG_TEST_SIMACDLY,
         /// Special test mode for enabling saturation on transmit
        TxSaturate = CFG_TEST_TXSATURATE,
         /// Special test mode DAT for first or peak arrival
        Arrival = CFG_TEST_ARRIVAL,
    }
}

mmp_enum! {
    /// Version config susystem flags
    ConfigVersionField : cfg_version_param_enum_t {
     /// Software application name string
        Name = CFG_VERSION_SWAPPNAME,
         /// Software version string
        SwVersion = CFG_VERSION_SWVERSION,
         /// Configuration database version string
        DbVersion = CFG_VERSION_DBVERSION,
    }
}

mmp_enum! {
    /// Xponder config subsystem flags
    ConfigXpndField : cfg_xpnd_param_enum_t {
     /// Frequency to listen for response pings (applicable only to platforms without MultiRx capability)
        RxFreq = CFG_XPND_RXFREQ,
         /// Ping response lockout, in milliseconds
        RxLockout = CFG_XPND_RXLOCKOUT,
         /// Receive pulse width, in milliseconds
        RxToneDuration = CFG_XPND_RXTONEDUR,
         /// Transpond turn-around time at remote node, in tenths of a millisecond
        TurnAroundTime = CFG_XPND_TAT,
         /// Interrogation pulse width, in tenths of a millisecond
        TxToneDuration = CFG_XPND_TXTONEDUR,
         /// Receive detection threshold for transponder pings, in standard deviations above mean noise level
        RxThreshold = CFG_XPND_RXTHRESH,
         /// The AGC level that should be set when the avg. background noise energy in the center of the band is at 1.  Lower values lower the noise floor allowing for more signal head-room; higher values raise it allowing more resolution for detecting weak signals.
        AGCLevel = CFG_XPND_AGCREF,
          /// Response tone sent on reception of tone set by CFG_XPND_RXFREQ after delay of CFG_XPND_TAT.
        ResponseFreq = CFG_XPND_RESPFREQ,
         /// Defines the action taken on the reception of a downward HFM chirp  9 CFG_XPND_XPNDEMUTAT transponder emulator response turnaround time in tenths of a millisecond  10 CFG_XPND_XPNDEMUMODE transponder emulator mode  11 CFG_XPND_HPR400CHAN Set the HPR400 channel  12 CFG_XPND_RESPONDER Enable Pulse responder mode  13 CFG_XPND_CHIRP_RESP define response to be requested as a reply to at%rr command  14 CFG_XPND_BANDWIDTH set bandwidth for processing  15 CFG_XPND_LOGRESULTS Enable logging to data logger
        LBLMode = CFG_XPND_LBLMODE,
    }
}

mmp_enum! {
    /// Nav config subsection flags
    ConfigNavField : cfg_nav_param_enum_t {
     /// Latitude  in millionths of a degree
        Latitude = CFG_NAV_LATITUDE,
         /// Longitude in millionths of a degree
        Longitude = CFG_NAV_LONGITUDE,
         /// absolute altitude relative to WGS84
        GPSAltitude = CFG_NAV_GPSALT,
         /// altitude above sea floor in meters
        Altitude = CFG_NAV_ALTITUDE,
         /// depth below sea level in meters
        Depth = CFG_NAV_DEPTH,
         /// compass bearing in degrees
        Compass = CFG_NAV_COMPASS,
         /// pitch in degrees
        Pitch = CFG_NAV_PITCH,
         /// roll in degrees
        Roll = CFG_NAV_ROLL,
         /// speed of sound in m/s
        SoundSpeed = CFG_NAV_CSOUND,
         /// additional data fields for position information
        ReplyData = CFG_NAV_REPLY_DATA,
         /// offset of compass heading to vehicle center line
        HeadingOffset = CFG_NAV_HEADOFFSET,
         /// Enable one-way ranging on data transmissions when sychronized to external 1PPS sources
        SyncRanging = CFG_NAV_SYNCRANGING,
         /// offset of AHRS pitch to transducer plane
        PitchOffset = CFG_NAV_PITCHOFFSET,
         /// offset of AHRS roll to transducer plane
        RollOffset = CFG_NAV_ROLLOFFSET,
         /// The type of GPS sentence, if any, that may be used to adjust the modem's system clock
        GpsSyncMessage = CFG_NAV_GPSSYNCMSG,
    }
}

mmp_enum! {
    /// Recorder config subsection flags
    ConfigRecorderField : cfg_recorder_param_enum_t {
     /// recording mode for automatic recording
        RecMode = CFG_RECORDER_RECMODE,
         /// format of names for recorded audio files
        NameFormat = CFG_RECORDER_NAMEFORMAT,
    }
}

mmp_enum! {
    /// Sonar config subsystem flags
    ConfigSonarField: cfg_sonar_param_enum_t {
     /// UNDOCUMENTED
        ToneDetect = CFG_SONAR_TONE_DETECT,
         /// UNDOCUMENTED
        ToneMin = CFG_SONAR_TONE_MIN,
         /// UNDOCUMENTED
        ToneMax = CFG_SONAR_TONE_MAX,
         /// UNDOCUMENTED
        ToneThreshold = CFG_SONAR_TONE_THRESH,
         /// analysis on or off
        NoiseAnalysis = CFG_SONAR_NOISE_ANALYSIS,
         /// UNDOCUMENTED
        AGCInit = CFG_SONAR_AGC_INIT,
         /// real only samples or carrier moved to complex base-band
        Frontend = CFG_SONAR_FRONTEND,
         /// analysis band width in Hz
        Bandwidth = CFG_SONAR_BANDWIDTH,

    }
}

mmp_enum! {
    /// SDK config subsystem flags
    ConfigSdkField :cfg_usersdk_param_enum_t {
     /// UNDOCUMENTED
        Sdk =  CFG_USER_SDK_N,
    }
}

mmp_enum! {
    /// Transport config subsection flags
    ConfigTransportField : cfg_transport_param_enum_t {
     /// Global enable or disable for transport layer activity
        L4Enable = CFG_TRANSPORT_L4ENB,
         /// Enable mode for transport layer addressing information (force always on or track with InputMode)
        TPMode = CFG_TRANSPORT_TPMODE,
         /// Transport address to be applied to transmitted packets originating on UART 0
        SrcP1 = CFG_TRANSPORT_SRCP1,
         /// Transport address to be applied to transmitted packets originating on UART 1
        SrcP2 = CFG_TRANSPORT_SRCP2,
         /// Delivery destination for received packets tagged with transport address 1
        Dst1 = CFG_TRANSPORT_DST1,
         /// Delivery destination for received packets tagged with transport address 2
        Dst2 = CFG_TRANSPORT_DST2,
         /// Delivery destination for received packets tagged with transport address 3
        Dst3 = CFG_TRANSPORT_DST3,
         /// Delivery destination for received packets tagged with transport address 4
        Dsr4 = CFG_TRANSPORT_DST4,
    }
}

mmp_enum! {
    /// WHOI config subsytem flags
    ConfigWhoiField : cfg_whoi_param_enum_t {
     /// Display RXD message
        Rxd = CFG_WHOI_RXD,
         /// Display RXA message
        Rxa = CFG_WHOI_RXA,
         /// Heartbeat Cycle Timing
        CycleTiming = CFG_WHOI_CYCTO,
         /// Host Data Timing
        HostDataTiming = CFG_WHOI_DATO,
         /// Acoustic Data Timing
        AcousticDataTiming = CFG_WHOI_PKTTO,
    }
}

mmp_enum! {
    /// Analog input config subsection flags
    ConfigAinField :cfg_ain_param_enum_t {
     /// time interval for automated polling
        PollRate = CFG_AIN_POLLRATE,
         /// number of measurements on chan 1
        Ch1NumMeasurements = CFG_AIN_1NMEAS,
         /// delay in 1st reading after turn-on (ch1)
        Ch1StartDelay = CFG_AIN_1STRTDLY,
         /// delay in each additional reading (ch1)
        Ch1Delay = CFG_AIN_1RPTDLY,
         /// type of sensor on chan 1
        Ch1SensorType = CFG_AIN_1TYPE,
         /// number of measurements on chan 2
        Ch2NumMeasurements =CFG_AIN_2NMEAS,
         /// delay in 1st reading after turn-on (ch2)
        Ch2StartDelay =CFG_AIN_2STRTDLY,
         /// delay in each additional reading (ch2)
        Ch2Delay =CFG_AIN_2RPTDLY,
         /// type of sensor on chan 2
        Ch2SensorType =CFG_AIN_2TYPE,
         /// number of measurements on chan 3
        Ch3NumMeasurements =CFG_AIN_3NMEAS,
         /// delay in 1st reading after turn-on (ch3)
        Ch3StartDelay =CFG_AIN_3STRTDLY,
         /// delay in each additional reading (ch3)
        Ch3Delay =CFG_AIN_3RPTDLY,
         /// number of measurements on chan 4
        Ch4NumMeasurements =CFG_AIN_4NMEAS,
         /// delay in 1st reading after turn-on (ch4)
        Ch4StartDelay =CFG_AIN_4STRTDLY,
         /// delay in each additional reading (ch4)
        Ch4Delay =CFG_AIN_4RPTDLY,
         /// number of measurements on chan 5
        Ch5NumMeasurements =CFG_AIN_5NMEAS,
         /// delay in 1st reading after turn-on (ch5)
        Ch5StartDelay =CFG_AIN_5STRTDLY,
         /// delay in each additional reading (ch5)
        Ch5Delay =CFG_AIN_5RPTDLY,
         /// number of measurements on chan 6
        Ch6NumMeasurements =CFG_AIN_6NMEAS,
         /// delay in 1st reading after turn-on (ch6)
        Ch6StartDelay =CFG_AIN_6STRTDLY,
         /// delay in each additional reading (ch6)
        Ch6Delay =CFG_AIN_6RPTDLY,

    }
}

mmp_enum! {
    /// Pressure sensor config subsystem flags
    ConfigPressureField :cfg_pressure_param_enum_t{
      /// Analog input channel where the pressure transducer is connected, or 0 if none
        AnalogInputChannel = CFG_PRESSURE_AINCHNL,
         /// Conversion factor for pressure, number of meters per psi
        MetersPerPsi = CFG_PRESSURE_METERSPERPSI,
         /// Atmospheric pressure in PSI to be subtracted from pressure reading when converting to depth below sea level
        AtmOffset = CFG_PRESSURE_ATMOFFSET,
         /// The pressure in pounds per square inch (psi) as set by user or updated from pressure gauge
        PressurePsi = CFG_PRESSURE_PSI,
    }
}

mmp_enum! {
    /// DPSK config susystem flags
    ConfigDpskField : cfg_dpsk_param_enum_t {
     /// DPSK board enable - control for the feature
        DpskEnable = CFG_DPSK_DPSKBOARD,
         /// Logging of DPSK Data
        DpskLogging = CFG_DPSK_DPSKLOGGING,
         /// DPSK Acoustic Ouput Format
        DpskAcousticFormat = CFG_DPSK_DPSKACFORMAT,
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum ParameterType {
    /// Interface parameters
    IFace(IFaceField),
    /// S-Register parameters
    SReg(SRegField),
    /// Deck box-specific data/notifications
    Deckbox(DeckboxField),
    /// Data packets and other general notifications
    Data(DataField),
    ///Transponding and ranging data/notifications
    Transpond(TranspondField),
    ///Time- and date-related data/notifications";
    Time(TimeField),
    ///Special subsystem for retrieving all values in a config section: set the field with the CFG section number to get or
    /// MMP_SUBSYS_CFG_ALL to get all sections.  If used with a SET operation and MMP_SUBSYS_CFG_ALL is used for both the
    /// the subsys and field, the configuration database will be written to flash.
    ConfigAll,
    ///Co-processor board;
    ConfigCoproc(ConfigCoprocField),
    ///On-board datalogger
    ConfigDatalogger(ConfigDataloggerField),
    ///Modem functionality
    ConfigModem(ConfigModemField),
    ///Release functionality
    ConfigRelease(ConfigReleaseField),
    ///UART paramters
    ConfigSerial(ConfigSerialField),
    ///System parameters
    ConfigSystem(ConfigSystemField),
    ///Test functionality
    ConfigTest(ConfigTestField),
    ///Version Information
    ConfigVersion(ConfigVersionField),
    /// Transpond control
    ConfigXpnd(ConfigXpndField),
    /// Global pose sensors
    ConfigNav(ConfigNavField),
    ///Data recorder configuration
    ConfigRecorder(ConfigRecorderField),
    /// Sonar modem configuration
    ConfigSonar(ConfigSonarField),
    ///Sonar modem user configuration
    ConfigSdk(ConfigSdkField),
    /// Transport layer routing/tagging information
    ConfigTransport(ConfigTransportField),
    /// WHOI configuration
    ConfigWhoi(ConfigWhoiField),
    /// Analog input option
    ConfigAin(ConfigAinField),
    /// Pressure gauge options
    ConfigPressure(ConfigPressureField),
    /// USBL control section
    ConfigUsbl,
    /// NUWC ranging dpsk integration section
    ConfigDpsk(ConfigDpskField),
}

/// Parameter Group structure for Get/Set/Notify messages
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct Parameter<'a> {
    pub parameter: ParameterType,
    pub data: &'a [u8],
}

/// Parse a Parameter type from a byte stream
///
/// Does not actually *parse* the data, but returns the Parameter struct that
/// will identify what kind of parameter it is and the data payload.
///
/// # Example
/// ```
/// use benthos_mmp::parameter::*;
///
/// assert_eq!(
///     parse_parameter(b"\x42\x0D\x00\x00"),
///     Ok((
///         &b""[..],
///         Parameter{
///             parameter: ParameterType::ConfigModem(ConfigModemField::TxRate),
///             data: &b""[..]
///         }
///     ))
/// );
/// assert_eq!(
///     parse_parameter(b"\x02\x00\x00\x02\x80\x00"),
///     Ok((
///         &b""[..],
///         Parameter{
///             parameter: ParameterType::Deckbox(DeckboxField::SpeakerVol),
///             data: &b"\x80\x00"[..]
///         }
///     ))
/// );
/// assert_eq!(
///     parse_parameter(b"\x00\x02\x00\x04\x00\x01\x00\x00"),
///     Ok((
///         &b""[..],
///         Parameter{
///             parameter: ParameterType::IFace(IFaceField::Version),
///             data: &b"\x00\x01\x00\x00"[..]
///         }
///     ))
/// );
/// ```
pub fn parse_parameter(i: &[u8]) -> nom::IResult<&[u8], Parameter> {
    let (i, subsystem) =
        nom::combinator::map_res(nom::number::complete::be_u8, SubSystem::try_from)(i)?;
    let (i, parameter) = nom::combinator::map_res(
        nom::number::complete::be_u8,
        |u| -> Result<ParameterType, u8> {
            match subsystem {
                SubSystem::IFace => Ok(ParameterType::IFace(IFaceField::try_from(u)?)),
                SubSystem::SReg => Ok(ParameterType::SReg(SRegField::try_from(u)?)),
                SubSystem::Deckbox => Ok(ParameterType::Deckbox(DeckboxField::try_from(u)?)),
                SubSystem::Data => Ok(ParameterType::Data(DataField::try_from(u)?)),
                SubSystem::Transpond => Ok(ParameterType::Transpond(TranspondField::try_from(u)?)),
                SubSystem::Time => Ok(ParameterType::Time(TimeField::try_from(u)?)),
                SubSystem::CfgAll => Ok(ParameterType::ConfigAll),
                SubSystem::CfgCoproc => {
                    Ok(ParameterType::ConfigCoproc(ConfigCoprocField::try_from(u)?))
                }
                SubSystem::CfgDatalog => Ok(ParameterType::ConfigDatalogger(
                    ConfigDataloggerField::try_from(u)?,
                )),
                SubSystem::CfgModem => {
                    Ok(ParameterType::ConfigModem(ConfigModemField::try_from(u)?))
                }
                SubSystem::CfgRelease => Ok(ParameterType::ConfigRelease(
                    ConfigReleaseField::try_from(u)?,
                )),
                SubSystem::CfgSerial => {
                    Ok(ParameterType::ConfigSerial(ConfigSerialField::try_from(u)?))
                }
                SubSystem::CfgSystem => {
                    Ok(ParameterType::ConfigSystem(ConfigSystemField::try_from(u)?))
                }
                SubSystem::CfgTest => Ok(ParameterType::ConfigTest(ConfigTestField::try_from(u)?)),
                SubSystem::CfgVersion => Ok(ParameterType::ConfigVersion(
                    ConfigVersionField::try_from(u)?,
                )),
                SubSystem::CfgXpnd => Ok(ParameterType::ConfigXpnd(ConfigXpndField::try_from(u)?)),
                SubSystem::CfgNav => Ok(ParameterType::ConfigNav(ConfigNavField::try_from(u)?)),
                SubSystem::CfgRecorder => Ok(ParameterType::ConfigRecorder(
                    ConfigRecorderField::try_from(u)?,
                )),
                SubSystem::CfgSonar => {
                    Ok(ParameterType::ConfigSonar(ConfigSonarField::try_from(u)?))
                }
                SubSystem::CfgSdk => Ok(ParameterType::ConfigSdk(ConfigSdkField::try_from(u)?)),
                SubSystem::CfgTransport => Ok(ParameterType::ConfigTransport(
                    ConfigTransportField::try_from(u)?,
                )),
                SubSystem::CfgWhoi => Ok(ParameterType::ConfigWhoi(ConfigWhoiField::try_from(u)?)),
                SubSystem::CfgAin => Ok(ParameterType::ConfigAin(ConfigAinField::try_from(u)?)),
                SubSystem::CfgPressure => Ok(ParameterType::ConfigPressure(
                    ConfigPressureField::try_from(u)?,
                )),
                SubSystem::CfgUsbl => Ok(ParameterType::ConfigUsbl),
                SubSystem::CfgDpsk => Ok(ParameterType::ConfigDpsk(ConfigDpskField::try_from(u)?)),
            }
        },
    )(i)?;

    let (i, data) = nom::multi::length_data(nom::number::complete::be_u16)(i)?;

    Ok((i, Parameter { parameter, data }))
}
