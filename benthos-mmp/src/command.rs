use crate::ffi;
use crate::mmp_enum;

use core::convert::TryFrom;

mmp_enum! {
    /// Command Type
    ///
    /// Used in Exec messages
    CommandSect : mmp_cmdsect_t {
        /// Null command section (unused)
        Null = MMP_CMDSECT_NULL,
        /// Standard commands and operations supported on most modem-based platforms
        Std = MMP_CMDSECT_STD,
        /// Ranging and releasing operations
        RngRls = MMP_CMDSECT_RNGRLS,
        /// Commands for Data Logger
        Datalog = MMP_CMDSECT_DATALOG,
    }
}

mmp_enum! {
    /// STD Command Section Commands
    StdCommand: mmp_std_cmd_t {
        /// Obtain modem build info (similar to ATI shell command)
        Info = MMP_STD_CMD_INFO,
        ///  Initiate an acoustic MMP request to another modem.  Remote MMP request should be embedded in the data field (no @ sentinel needed). Not implemented on networking builds.
        RemoteReq = MMP_STD_CMD_REMOTE_MMPREQ,
        /// Save S-registers to flash (soon to be deprecated)
        FlashSRegs = MMP_STD_CMD_FLASH_SREGS,
        /// Reset S-registers to factory defaults (soon to be deprecated)
        ResetSRegs = MMP_STD_CMD_RESET_SREGS,
        /// Set acoustic baud rate on remote modem
        RemoteSetAcousticBaud = MMP_STD_CMD_REMOTE_SETACOUBAUD,
        /// Set transmit power on remote modem
        RemoteSetTxPower = MMP_STD_CMD_REMOTE_SETTXPOWER,
        /// Get S-registers from remote modem (soon to be deprecated)
        RemoteGetSRegs = MMP_STD_CMD_REMOTE_GETSREGS,
        /// Send data packet to remote modem
        RemoteSendData = MMP_STD_CMD_REMOTE_SENDDATA,
        /// Test acoustic link with remote modem
        RemoteTestLink = MMP_STD_CMD_REMOTE_TESTLINK,
        /// Begin firmware update cycle
        UpdateFirmware = MMP_STD_CMD_UPDATE_FIRMWARE,
        /// Reboot the modem
        Reset = MMP_STD_CMD_RESET,
        /// Get battery levels on remote modem
        RemoteGetBattery = MMP_STD_CMD_REMOTE_GETBATT,
        /// Issue a compact modem wakeup sequence to a remote compact modem
        RemoteWakeup = MMP_STD_CMD_REMOTE_CMWAKEUP,
        /// Obtain the AGC history for the last 10 seconds from a remote modem
        RemoteGetAGCHistory = MMP_STD_CMD_REMOTE_GETAGCHIST,
        /// Initiate an auto-baud sequence to a remote modem to set an optimal acoustic baud rate between them
        RemoteAutobaud = MMP_STD_CMD_REMOTE_AUTOBAUD,
        /// Obtain battery charge from remote units with IBPS smart batteries
        RemoteGetBatteryCharge = MMP_STD_CMD_REMOTE_GETBATTCHG,
        /// Send command packet to remote modem
        RemoteSendCommand = MMP_STD_CMD_REMOTE_SENDCMD,
        /// Obtain acceleration and tilt values for X/Y/Z axis if function supported on board.  HW_NOT_PRESENT error results if board has no accelerometer.
        TiltAccel = MMP_STD_CMD_TILT_ACCEL,
        /// Obtain overall unit tilt value based upon @TiltAxis configuration parameter.  HW_NOT_PRESENT error results if board has no accelerometer.
        AxisTilt = MMP_STD_CMD_AXIS_TILT,
        /// Reset the battery metering information on this platform, if applicable.
        ResetBatteryMetering = MMP_STD_CMD_RESET_BATT_METERING,
        /// Obtain metering information for the local battery, if supported
        BatteryMeter = MMP_STD_CMD_BATT_METER,
        /// Get battery metering information from a remote unit, if supported
        RemoteGetBatteryMeter = MMP_STD_CMD_REMOTE_GETMETER,
        /// Get battery levels from the local modem
        GetBatteryLevel = MMP_STD_CMD_GETBATT,
        /// Obtain battery charge from a local modem with IBPS smart batteries
        GetBatteryCarge = MMP_STD_CMD_GETBATTCHG,
        /// Get the local AGC history buffer
        GetAGCHistory = MMP_STD_CMD_GETAGCHIST,
        /// Gets the value of a single S-register on a remote modem
        RemoteGetOneSReg = MMP_STD_CMD_REMOTE_GETONESREG,
        /// Sets and stores a single S-register on a remote modem; response is ACK or ERROR
        RemoteSetOneSReg = MMP_STD_CMD_REMOTE_SETONESREG,
        /// Reset a remote modem with a low-level reset instruction (no acknowledgement from remote)
        RemoteReset = MMP_STD_CMD_REMOTE_RESET,
        /// Obtain the board temperature from one of the built-in thermistors (one on ATM board sets, two on UDB board sets)
        BoardTemp = MMP_STD_CMD_BOARD_TEMP,
        /// Send a serial break to remote modem
        RemoteSendBreak = MMP_STD_CMD_REMOTE_SENDBREAK,
        /// move a remote modem's address into a new address group
        RemoteSetGroup = MMP_STD_CMD_SET_REMOTE_GROUP,
    }
}

mmp_enum! {
    /// STD Command Section Commands
    RngRlsCommand: mmp_rngrls_cmd_t {
        ///Issue interrogation or silent ping and enter transpond receive mode for the system default amount of time
        Transpond = MMP_RNGRLS_CMD_TRANSPOND,
        ///Obtain the range to another modem
        Range = MMP_RNGRLS_CMD_RANGE,
        ///Check the release mechanism status on a remote modem
        ReleaseStatus = MMP_RNGRLS_CMD_RELEASE_STATUS,
        ///Issue a release command to a remote Benthos SMART release (SR-50, SR-100, etc.)
        SmartRelease = MMP_RNGRLS_CMD_SMART_RELEASE,
        ///Issue a release command to a remote Benthos SMART modem (SM-75, OEM boardset w/ burn wire, etc.)
        Burnwire = MMP_RNGRLS_CMD_BURNWIRE,
        ///Issue a release command to a remote Benthos FSK release (Model 865, etc.)
        FskRelease = MMP_RNGRLS_CMD_FSK_RELEASE,
        ///Issue a command to an EdgeTech/ORE/EG&G release
        EdgetechRelease = MMP_RNGRLS_CMD_EDGETECH_RELEASE,
        ///Issue a command to a University of Rhode Island release/device
        UriRelease = MMP_RNGRLS_CMD_URI_RELEASE,
        ///Save per-frequency transpond sensitivity threshold adjustments to flash
        FlashXpndAdj = MMP_RNGRLS_CMD_FLASH_XPNDADJ,
        ///Obtain bearing from a DAT
        Bearing = MMP_RNGRLS_CMD_BEARING,
        ///Issue a command to a GeoPro (Germany) release unit
        GeoProRelease = MMP_RNGRLS_CMD_GEOPRO_RELEASE,
        ///Obtain position information from a remote modem
        NavData = MMP_RNGRLS_CMD_NAVDATA,
        ///probe channel for multipath
        ChannelProbe = MMP_RNGRLS_CMD_CHANNEL_PROBE,
        ///Ranging specific to a Teledyne Benthos next-gen release
        RSeriesRange = MMP_RNGRLS_CMD_RSERIES_RANGE,
        ///Activate an R Series release
        RSeriesActivate = MMP_RNGRLS_CMD_RSERIES_ACTIVATE,
        ///Hibernate an R Series release
        RSeriesHibernate = MMP_RNGRLS_CMD_RSERIES_HIBERNATE,
        ///Set the transpond reSponse mode in a remote R Series unit
        RSeriesXpndMode = MMP_RNGRLS_CMD_RSERIES_XPND_MODE,
        ///Set the transpond reSponse mode in a remote standard unit
        StdXpndMode = MMP_RNGRLS_CMD_STD_XPND_MODE,
        ///Enable R Series transponding
        RSeriesDbUnlock = MMP_RNGRLS_CMD_RSERIES_DB_UNLOCK,
        ///Disable R Series transponding
        RSeriesDbLock = MMP_RNGRLS_CMD_RSERIES_DB_LOCK,
        ///Issue a release command to a remote Benthos R Series release
        RSeriesRelease = MMP_RNGRLS_CMD_RSERIES_RELEASE,
        ///Issue pulse on binary out 1 to a responder
        ResponderRange = MMP_RNGRLS_CMD_RESPONDER_RANGE,
        ///Issue interrogation or silent ping and enter transpond receive mode for a specified amount of time
        TranspondWithTimeout = MMP_RNGRLS_CMD_TRANSPOND_W_TIMEOUT,
        ///Issue a range request with a timeout specified (not system @AcRspTmOut default)
        RangeWithTimeout = MMP_RNGRLS_CMD_RANGE_W_TIMEOUT,
        ///Enter or exit spectrum analysis mode, with update rate in 0.5 second increments (or 0 to disable)
        SpectrumMode = MMP_RNGRLS_CMD_SPECTRUM_MODE,
        ///Request the high precision absolute and relative bearing
        AbsRelBearing = MMP_RNGRLS_CMD_ABSREL,
    }
}

mmp_enum! {
    DatalogCommand : mmp_datalog_cmd_t {
        ///Clears the data logger buffer
        Clear = MMP_DATALOG_CMD_CLEAR,
        ///Report the number of bytes in the data logger
        Size = MMP_DATALOG_CMD_SIZE,
        ///Read back the data logger buffer
        ReadData = MMP_DATALOG_CMD_READ_DATA,
        ///Read back a 4 kbyte page
        ReadPage = MMP_DATALOG_CMD_READ_PAGE,
        ///Read back a 256 byte sector
        ReadSector = MMP_DATALOG_CMD_READ_SECTOR,
        ///Write data in to the data logger
        WriteData = MMP_DATALOG_CMD_WRITE_DATA,
        ///Set the read pointer to a specific location
        Seek = MMP_DATALOG_CMD_SEEK,
        ///Report the position of the read pointer
        Tell = MMP_DATALOG_CMD_TELL,
        ///Retrieve general information about the local or remote data logger
        List = MMP_DATALOG_CMD_LIST,
        ///Find records in the local or remote data logger using the command line query syntax
        Find = MMP_DATALOG_CMD_FIND,
        ///Dump the local or remote data logger based on byte offset and length (on all-source or persource basis)
        Dump = MMP_DATALOG_CMD_DUMP,
        ///Get information pertaining to one or more data sources in the logger (total bytes and number of records)
        SrcInfo = MMP_DATALOG_CMD_SRCINFO,
        /// Request a block of up to 4K from a remote modem's data logger with bit flags indicating which cells are valid/corrupted
        GetDatalogBlock = MMP_DATALOG_CMD_REMOTE_GETDLOGBLK,
    }
}
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum CommandType {
    /// Unused command type
    Null,
    /// Standard commands
    Std(StdCommand),
    /// Ranging and Release commands
    RngRelease(RngRlsCommand),
    /// Datalog commands
    Datalog(DatalogCommand),
}

/// Command structure for Get/Set/Notify messages
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct Command<'a> {
    pub command: CommandType,
    pub data: &'a [u8],
}

/// Parse a Command type from a byte stream
///
/// Does not actually *parse* the data, but returns the Parameter struct that
/// will identify what kind of parameter it is and the data payload.
///
/// # Example
/// ```
/// use benthos_mmp::command::*;
///
/// assert_eq!(
///     parse_command(b"\x02\x01\x00\x02\x00\x0A"),
///     Ok((
///         &b""[..],
///         Command{
///             command: CommandType::RngRelease(RngRlsCommand::Range),
///             data: &b"\x00\x0A"[..]
///         }
///     ))
/// );
/// ```
pub fn parse_command(i: &[u8]) -> nom::IResult<&[u8], Command> {
    let (i, command_section) =
        nom::combinator::map_res(nom::number::complete::be_u8, CommandSect::try_from)(i)?;
    let (i, command) = nom::combinator::map_res(
        nom::number::complete::be_u8,
        |u| -> Result<CommandType, u8> {
            match command_section {
                CommandSect::Std => Ok(CommandType::Std(StdCommand::try_from(u)?)),
                CommandSect::RngRls => Ok(CommandType::RngRelease(RngRlsCommand::try_from(u)?)),
                CommandSect::Datalog => Ok(CommandType::Datalog(DatalogCommand::try_from(u)?)),
                CommandSect::Null => Ok(CommandType::Null),
            }
        },
    )(i)?;

    let (i, data) = nom::multi::length_data(nom::number::complete::be_u16)(i)?;

    Ok((i, Command { command, data }))
}
