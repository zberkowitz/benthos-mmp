//! Benthos MMP FFI definitions
#[cfg(not(target_arch = "arm"))]
pub use benthos_mmp_sys::*;
#[cfg(target_arch = "arm")]
pub use benthos_mmp_thumbv6m_sys::*;
