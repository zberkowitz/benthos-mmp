# Benthos Multi Modem Protocol

# Optional Features

*By default no features are enabled*

- **std**:  Enable use of rust stdlib.  Also enables "std" feature of `nom`
- **alloc**: Enable global allocation features (e.g. Box<T>, Vec<T>, subset of stdlib).  Also enables "alloc" features of `nom`
- **mmp-sys**: Use `benthos-mmp-sys` crate for API constants.  Requires building with `nightly` and `-Zbuild_dep` until
[cargo#7915](https://github.com/rust-lang/cargo/issues/7915) is resolved.
