//! Rust bindgen generated definitions for the Benthos Modem Management Protocol
//!
//! Original header: mmp_c_defs.h
//! ```c
//! /*****************************************************************************************
//! * Modem Management Protocol (MMP) Definition Header File
//! *
//! * Copyright (C) 2018 Teledyne Marine Systems
//! * All Rights Reserved
//! *
//! * Automatically generated on 06/13/2018 at 10:05:11
//! *****************************************************************************************/
//! ```
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![no_std]
include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
