# benthos-mmp-sys: Benthos Modem Management Protocol Bindings

The Benthos Modem Management Protocol is a communication protocol for controlling
Benthos acoustic modems.  This crate provides rust bindings generated using the
`bindgen` crate and the vendor supplied `mmp_c_def.h` C header file.

# Build Requirements
## Windows
Building this crate on windows requires LLVM to be installed.  Follow the
[guidelines](https://rust-lang.github.io/rust-bindgen/requirements.html) in the 
`buildgen` docs to download LLVM and set the `LIBCLANG_PATH` as proscribed.

# #![no_std] 
This crate is `#![no_std]` and can be used in embedded environments.
