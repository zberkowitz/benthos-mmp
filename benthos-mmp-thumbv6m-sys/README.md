# benthos-mmp-thumbv6m-sys: Benthos Modem Management Protocol Bindings

The Benthos Modem Management Protocol is a communication protocol for controlling
Benthos acoustic modems.  This crate provides rust bindings generated using the
`bindgen` crate and the vendor supplied `mmp_c_def.h` C header file for the
`thumbv6m-none-eabi` target.

This awkward workaround is due to rust-stable unifying build-dependencies and
target-dependencies at the moment.  Work is underway to separate these concerns 
(see [cargo#8088](https://github.com/rust-lang/cargo/issues/8088)) but this requires
using rust nightly.

# #![no_std] 
This crate is `#![no_std]` and can be used in embedded environments.
